'use strict';
// TODO: FIX the code and add headers for documentation

var stablished = false;

var submitForm = (function(){
  var pathStr = '';
  var upload = function (iframe){
    // Send 'Upload' message to iframe
    iframe.contentWindow.postMessage('upload', '*');
  };

  var api = {
    path: function(_path_) {
      if (_path_) {
        pathStr = _path_;
      }
      return pathStr;
    },
    uploadAndSubmit: function() {
      var iframe = $('#myIframe')[0];
      if (stablished){
        if (pathStr.length !== 0){
          console.log(pathStr); // We show the isolates paths
          // Replace hidden input in the form with value of paths from server
          $('input[name="uploadpath"]').val(pathStr);
          // Submit data to server/pipeline/webface/
          $( '#theform' ).submit();
        }else{
          console.log('Path empty', 'We upload');
          upload(iframe);
        }
      }else{
        console.log('Connection with iframe not stablished');
      }
    }
  };

  return api;
})();

var onIFrameLoad = function (){
  console.log('iframe loaded');
  window.iframeLoaded = true;
};

// Always do something if the document is ready
$(document).ready(function() {


   var addIframeListener = function() {
      var iframe = $('#myIframe')[0]
      if(typeof iframe !== 'undefined'){
         // Check to validate that the iframe does exist //ADDED BY MARTIN
         iframe.onload = function() {
            console.log('iframe loaded');
            connectToIFrame(iframe);
         };
      }else{
         console.log('The iframe "myIframe" was not found');
      }
   };

	var connectToIFrame = function(iframe) {
		iframe.contentWindow.postMessage('connect', '*');
    stablished = true;

	};

  addIframeListener();

 // We need to be careful of a race condition. iFrame may have already loaded so we would never get an onLoad
 if (window.iframeLoaded) {
   var iframe = $('#myIframe')[0];
   console.log(iframe);
   connectToIFrame(iframe);
 }

	addEvent(window, 'message', function(e) {
	  
		var response = $.parseJSON(e.data);
      console.log('We get a new message', response.message[0]);
		submitForm.path(response.message[0]);
    // We receive the pathh for the isolates
    /*
      if (submitForm.path().length === 0){
      		  submitForm.path(response.message[0]);
          }
*/
    submitForm.uploadAndSubmit();
	});

  // Overwrite submit button
  $('input[value="Submit"]').attr('onclick', 'submitForm.uploadAndSubmit();');

});
