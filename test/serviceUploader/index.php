<?php #! /usr/bin/php5 -q
################################################################################
#                        CGE SERVICE - spaTyper                         #
################################################################################
# CONFIG VARIABLE
$service = "spaTyper"; # EDIT! SERVICE
$version = "1.0"; # EDIT! VERSION
$author = "'CGE Support','cgehelp'"; # EDIT! AUTHOR # FORMAT='Full Name','email'
$config = "config.cf"; # EDIT CONFIG FILE NAME

$serviceRoot = "/srv/www/htdocs/services/".$service."-".$version."/"; # SERVICE ROOT
$config = $serviceRoot.$config; # CONFIG PATH

?>

<!-- JavaScript Uploader Iframe -->
<div id="iframeWrapper">
<iframe onload="onIFrameLoad();" style="overflow:hidden;" id="myIframe" src="http://127.0.0.1:9000" height="320" width="1024" frameBorder="5" ></iframe>
</div>

<!-- START CONTENT -->
View the <a href="/services/spaTyper/history.php">version history</a> of this server. <br>


<!-- CHOOSE TECHNOLOGY TYPE -->
<p>
   <b>Sequencing Platform</b>
   <span style="color:#707070">Select the sequencing platform used to generate
   the uploaded reads. (Note: Select 'Assembled Genome' if you are uploading
   preassembled reads)</span><br>
   <!-- here starts the SELECT part -->
   <select name="technology" style="width:200px;">
   	<option value="Assembled_Genome">Assembled Genome/Contigs*</option>
   	<option value="454">454 - single end reads</option>
   	<option value="454_Paired_End_Reads">454 - paired end reads</option>
   	<option value="Illumina">Illumina - single end reads</option>
   	<option value="Paired_End_Reads">Illumina - paired end reads</option>
   	<option value="Ion_Torrent">Ion Torrent</option>
   	<option value="Solid">SOLiD - single end reads</option>
   	<option value="S_Mate_Paired_Reads">SOLiD - mate pair reads</option>
      <!--<option value="S_Paired_End_Reads">SOLiD - paired end reads</option>
      <option value="PAC_Bio">PacBio</option>-->
   </select>
</p>


<input type="button" onclick="submitForm.uploadAndSubmit();" value="Submit"></input>

<!-- END OF FORM -->
<?php
?>
<!-- NOTES -->
<br>
Here you can write a note if you like:
<ul>
<li><i>And even make a list </i><a href="#ref01">and link to something on the page</a> or <a href="http://www.google.com"> a different page</a>.</li>
<li><b>And the list can contain more lines</b></li>
</ul>

<br>

<hr><!-- REFERENCES -->

<h3>REFERENCES</h3>
<ol>
<li><a name="ref01">Here you can reference something</a>
</ol>

<hr><!-- CITATIONS -->

<h3>CITATIONS</h3>
<p>For publication of results, please cite:</p>
<ul>
  <li>
	Multilocus Sequence Typing of Total Genome Sequenced Bacteria.<br>
	Larsen MV, Cosentino S, Rasmussen S, Friis C, Hasman H, Marvig RL, Jelsbak L, Sicheritz-Pont�n T, Ussery DW, Aarestrup FM and Lund O.<br>
	J. Clin. Micobiol. 2012. 50(4): 1355-1361.<br>
	View the <a href="http://www.ncbi.nlm.nih.gov/pubmed/22238442">abstract</a>
  </li>
</ul>

<!-- END OF CONTENT -->
<?php
//$CGE->Javascripts($service, $version); // Printing Applet required Javascript!!
//$CGE->ReadTypeCheck(); // Printing the javescript required to test if the correct number of files were uploaded!!

// PIWIK WEBSITE TRAFIC TRACKING
// The Number in the following parenteses should match the PIWIK Website ID.
// To get the ID go to http://cge.cbs.dtu.dk/piwik/, login and then click on add new website in the bottom.
// Now the ID of all websites appear, and if this website is not in the list, then add it, and note down the ID, and use it below.

# STANDARD FOOTER
# First a simple headline like: "Support"
# Then a list of emails like this: "('Scientific problems','foo','foo@cbs.dtu.dk'),('Technical problems','bar','bar@cbs.dtu.dk')"
?>

<script type="text/javascript" src="/scripts/h5utils.js"></script>
<script type="text/javascript" src="/scripts/iframeConnectionService.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
