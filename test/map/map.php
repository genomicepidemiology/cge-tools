
<!-- Map Iframe -->
<div id="iframeWrapper">
  <iframe onload="onIFrameLoad();" style="overflow:hidden;" id="myIframe" name="myIframe" src="http://127.0.0.1:9000/#/analysisresult" height="700" width="1024" frameBorder="0"> </iframe>
</div>

<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="h5utils.js"></script>
<script type="text/javascript" src="iframeConnection.js"></script>
<!-- CITATIONS -  -><hr>

<h3>CITATIONS</h3>
<p>For publication of results, please cite:</p>
<ul>
  <li>
	Multilocus Sequence Typing of Total Genome Sequenced Bacteria.<br>
	Larsen MV, Cosentino S, Rasmussen S, Friis C, Hasman H, Marvig RL, Jelsbak L, Sicheritz-Pontén T, Ussery DW, Aarestrup FM and Lund O.<br>
	J. Clin. Micobiol. 2012. 50(4): 1355-1361.<br>
	View the <a href="http://www.ncbi.nlm.nih.gov/pubmed/22238442">abstract</a>
  </li>
</ul>
