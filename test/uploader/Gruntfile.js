// Generated on 2014-07-21 using generator-angular 0.7.1
'use strict';

// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// use this if you want to recursively match all subfolders:
// 'test/spec/**/*.js'

module.exports = function (grunt) {

  // Load grunt tasks automatically
  require('load-grunt-tasks')(grunt);

  // Time how long tasks take. Can help when optimizing build times
  require('time-grunt')(grunt);

  // Define the configuration for all the tasks
  grunt.initConfig({


    // The actual grunt server settings
    // connect: {
    //   options: {
    //     port: 9000,
    //     // Change this to '0.0.0.0' to access the server from outside.
    //     hostname: 'localhost',
    //     livereload: 35729
    //   },
    //   livereload: {
    //     options: {
    //       open: true,
    //       base: [
    //         '.tmp',
    //         './'
    //       ]
    //     }
    //   }
    // },


    php: {
        dist: {
            options: {
                port: 5001,
                base: '.', // folder from where the webserver will be served,
                keepalive: true,
            }
        }
    },
    // Automatically inject Bower components into the app
    'bower-install': {
      app: {
        html: 'uploader.php',
      }
    },

  });

  grunt.loadNpmTasks('grunt-php');

  grunt.registerTask('serve', function (target) {
    grunt.task.run([
      'bower-install',
      //'connect:livereload',
      'php'
    ]);
  });

};
