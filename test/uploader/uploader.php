<?php #! /usr/bin/php5 -q


################################################################################

# TODO: FIX this to allow development/production testing

################################################################################

################################################################################
#                            CGE SERVICE - CGE                                 #
################################################################################
# CONFIG VARIABLE
//phpinfo();
// $service = "CGEpipeline"; # EDIT! SERVICE
// $version = "1.0"; #EDIT! VERSION
// $author = "'Martin Thomsen','mcft'"; # EDIT! AUTHOR # FORMAT='Full Name','email'
// $config = "config_batch.cf"; #EDIT CONFIG FILE
//
// // $domain = 'https://cge.cbs.dtu.dk';
// $domain = 'localhost:5000';
// // $serviceRoot = "/srv/www/htdocs/services/".$service."-".$version."/"; #SERVICE ROOT
// $config = $serviceRoot.$config; #CONFIG PATH
//
// # STANDARD CBS PAGE TEMPLATES, always include this file
// include_once('/srv/www/php-lib/cge_std-2.0.php'); // Including CGE_std clases and functions
// // Load the CGE Class :: ARGUMENTS=($title, $meta, $banner,$css,$js)
// /* $CGE = new CGE('CGE Server','<base href="'.$domain.'">','/images/cge_buttons/banner.gif','','/services/CGEpipeline-1.0/fileUploaderFuncs_2.js,/services/CGEpipeline-1.0/h5utils.js'); */
//
// $CGE = new CGE('CGE Server','<base href="'.$domain.'">','/images/cge_buttons/banner.gif','','');
//
// # CGE MENU
// # Format is: ServerName, "(Link/Path.html, 'NameOfLink'),(Link/Path.html, 'NameOfLink')"
// $CGE->std_header("$service $version Batch Upload", "(batch.php,'Batch Upload'),(instructions.php,'Instructions'),(output.php,'Output'),(abstract.php,'Article abstract')"); // Print the Menu
//
// // REQUIRE THE USER TO LOGIN
// if($CGE->user_is_logged_in()){
//    $CGE->StartForm($config); // Start form (provide path to config file)
?>
<!-- START CONTENT -->

<!-- JavaScript Uploader Iframe -->
<div id="iframeWrapper">
<iframe onload="onIFrameLoad();" style="overflow:hidden;" id="myIframe" src="http://127.0.0.1:9000/#/" height="320" width="1024" frameBorder="5" ></iframe>
</div>


<!-- END OF FORM -->
<input type    = "button"
       value   = "Submit"
       OnClick = "submitForm.submit();"
       >
<input type    = "reset"
       value   = "Clear fields"
       >
</form>

<p><b>Disclaimer</b> <br>
<dfn>
   Data will be kept for comparison to previous and future submissions, but may
   be deleted by the submitter within 2 weeks of the submission. After this
   period the data will be exchanged with other publically availiable databases.
</dfn>
</p>

<!-- CITATIONS -  -><hr>

<h3>CITATIONS</h3>
<p>For publication of results, please cite:</p>
<ul>
  <li>
	Multilocus Sequence Typing of Total Genome Sequenced Bacteria.<br>
	Larsen MV, Cosentino S, Rasmussen S, Friis C, Hasman H, Marvig RL, Jelsbak L, Sicheritz-Pontén T, Ussery DW, Aarestrup FM and Lund O.<br>
	J. Clin. Micobiol. 2012. 50(4): 1355-1361.<br>
	View the <a href="http://www.ncbi.nlm.nih.gov/pubmed/22238442">abstract</a>
  </li>
</ul>
-->

<!-- bower:js -->
<script src="bower_components/jquery/dist/jquery.js"></script>
<script src="bower_components/es5-shim/es5-shim.js"></script>
<script src="bower_components/json3/lib/json3.min.js"></script>
<!-- endbower -->

<script type="text/javascript" src="h5utils.js"></script>
<script type="text/javascript" src="iframeConnection.js"></script>
