'use strict';

describe('Service: Geolocationservice', function () {

  // load the service's module
  beforeEach(module('batchUploadApp'));

  // instantiate service
  var Geolocationservice;
  beforeEach(inject(function (_Geolocationservice_) {
    Geolocationservice = _Geolocationservice_;
  }));

  it('should do something', function () {
    expect(!!Geolocationservice).toBe(true);
  });

});
