'use strict';

describe('Service: Uidservice', function () {

  // load the service's module
  beforeEach(module('batchUploadApp'));

  // instantiate service
  var Uidservice;
  beforeEach(inject(function (_Uidservice_) {
    Uidservice = _Uidservice_;
  }));

  it('should do something', function () {
    expect(!!Uidservice).toBe(true);
  });

});
