'use strict';

angular.module('batchUploadApp')
.service('ExcelParserService',
  function ExcelParserService($q, ExcelvalidationService, GeoLocationService) {
    // AngularJS will instantiate a singleton by calling 'new' on this function

    this.parseFile = function(file, checkFiles) {
      console.log('Parsing file...');
      // Task that will finish in the future
      var deferred = $q.defer();
      var promise = deferred.promise;
      var reader = new FileReader();
      var metadata = {};
      var fileNames = [];
      var excelErrors = {
        messages: [],
        nErrors: 0
      };
      var locationPromises = [];
      var isolatesToLocate = [];
      // We catch the load function of the reader and parse the excel file
      reader.onload = function(event) {
        var zip = new JSZip();
        var t = zip.load(event.target.result, {
          base64: false
        });
        var xlsx = XLSX.parseZip(t);
        var sheetNameList = xlsx.SheetNames;
        var json = {};
        // Parse excel to get file names
        sheetNameList.forEach(function(sheetName) {
          if (sheetName === 'Metadata') {
            var sheet = xlsx.Sheets[sheetName];
            var rObjArr = XLSX.utils.sheet_to_row_object_array(sheet);
            if (rObjArr.length > 0) {
              json[sheetName] = rObjArr;
              rObjArr.forEach(function(row, i) {
                // All fields (to avoid losing metadata with no content in
                // the excel sheet)
                var isolate = {
                  'sample_name': '',
                  'group_name': '',
                  'file_names': '',
                  'sequencing_platform': '',
                  'sequencing_type': '',
                  'pre_assembled': '',
                  'sample_type': '',
                  'organism': '',
                  'strain': '',
                  'subtype': '',
                  'country': '',
                  'region': '',
                  'city': '',
                  'zip_code': '',
                  'longitude': '',
                  'latitude': '',
                  'location_note': '',
                  'isolation_source': '',
                  'source_note': '',
                  'pathogenic': '',
                  'pathogenicity_note': '',
                  'collection_date': '',
                  'collected_by': '',
                  'usage_restrictions': '',
                  'usage_delay': 0,
                  'email_address': '',
                  'notes': ''
                };
                angular.extend(isolate, row);
                console.log(row);
                if (!ExcelvalidationService.emptyRow(isolate)) {
                  isolate.upload_dir = i+1;
                  isolate.batch = false;
                  if (isolate.latitude !== '' || isolate.longitude !== ''){
                    isolate.location_uncertainty_flag = 0;
                  }
                  // Validates if mandatory fields are present
                  var answer = ExcelvalidationService.isolate(
                    isolate, i+2,
                    checkFiles,
                    fileNames
                  );
                  if (answer.errors > 0 || excelErrors.nErrors > 0) {
                    if (excelErrors.nErrors !== 0){
                      excelErrors.messages = excelErrors.messages.concat(answer.message);
                    }else{
                      excelErrors.messages = answer.message;
                    }
                    excelErrors.nErrors += answer.errors;
                  }else{
                    // No error in this isolate
                    var files = isolate.file_names.split(' ');
                    files.forEach(function(fileName) {
                      metadata[fileName] = isolate;
                      fileNames.push(fileName);
                    });
                  }
                }else{
                  console.log('Row empty' ,row);
                }

              });
            }
          }
        });
        console.log(fileNames);
        if (fileNames.length === 0 && excelErrors.nErrors === 0){
          excelErrors.nErrors += 1;
          excelErrors.messages.push('The excel template is empty');
        }
        // Catch later by then. TODO: implement deferred.reject for errors
        deferred.resolve({
           'metadata': metadata,
           'files': fileNames,
           'errors': excelErrors
        });
      };

      reader.readAsBinaryString(file);
      return promise;
    };
  }
);
