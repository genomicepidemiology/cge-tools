'use strict';

angular.module('batchUploadApp')
  .factory('PostMessageService', function($rootScope) {

  var $messages = [];

  var api = {
    messages: function(_message_) {
      if (_message_) {
        $messages.push(_message_);
        $rootScope.$apply();
      }
      return $messages;
    },
    outgoing: function() {
      // better for perfomance reasons.
      //$rootScope.$emit('outgoingMessage');
      $rootScope.$broadcast('outgoingMessage');
    }
  };

  return api;
});
