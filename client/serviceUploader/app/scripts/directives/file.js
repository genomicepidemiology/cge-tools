'use strict';

angular.module('batchUploadApp')
.directive('file', function (ExcelParserService, UIDservice) {
  return {
    templateUrl: 'templates/uploadExcelFileTemplate.html',
    restrict: 'A',
    //scope: true, // Inherits from the parent's scope.
    link: function postLink(scope, element, attrs) {
       console.log('Init file directive...');
       // Excel variables
       scope.excelRight = false;

       var parseExcel = ExcelParserService.parseFile;

       element.find('input').bind('change', function(event){
          scope.errorsExist = false; // Hide alarms
          scope.isolates = [];
          scope.loadData = false; // First we remove everything
          //scope.spinner.startSpinner();
          scope.$apply();
          var files = event.target.files;
          var file = files[0];

          scope.testloaded = false;
          console.log('Parsing excel...'); // Uploading excel sheet

          if (!scope.excelRight){
             // Call service to process excel file. true => checking file names
             parseExcel(file, true).then(function(answer){
                if (answer.errors.nErrors > 0){
                   scope.errorsExist = true;
                   scope.multipleErrors = true;
                   scope.messages = answer.errors.messages;
                   scope.infoMessage = ' ' +
                      answer.errors.nErrors + ' ' +
                      'errors were detected in the Excel file: ' + file.name;
                   //scope.isolates = [];
                   //scope.spinner.stopSpinner();
                   //scope.waitingForData = true;
                   // Restore the input element when error
                   var inputElement = element.find('input');
                   inputElement.replaceWith(inputElement.val('').clone(true));
                }else{
                   scope.excelRight = true;
                   scope.errorsExist = false;

                   angular.extend(scope, answer);

                }
             });
          }
       });

       scope.removeExcelSheet = function (){

          if (scope.excelRight){
             // TODO: Service to go back to initial state.
             var inputElement = element.find('input');
             inputElement.replaceWith(inputElement.val('').clone(true));

             scope.excelRight = false;
             scope.excelExists = false;
             scope.errorsExist = false;
             scope.uploader.clearQueue(); // Remove excel means removing isolates
             scope.uploader.formData[0].UID = UIDservice.updateUID();

          }
       };

    }
  };
});
