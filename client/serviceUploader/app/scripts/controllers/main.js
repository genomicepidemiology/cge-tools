'use strict';

angular.module('batchUploadApp')
  .controller('MainCtrl', function ($scope, FileUploader, PostMessageService, ENV, UIDservice) {


    //////////////
    ////////////// TODO: FIX time between Batchs
    ////////////// TODO: WARNING when uploading more than 150 isolates per day
    ////////////// TODO: ... Ask martin again about this
    //////////////
    //////////////



    ///////////////////////////
    // Scope variables

    // General variables
    $scope.isCollapsed = true;
    $scope.infoMessage = '';       // Initially no info message
    $scope.queueMessage = '';       // Initially no info message
    $scope.errorsExist = false;
    $scope.multipleErrors = false;

    $scope.missingFiles = [];
    $scope.chunksFailed = [];


    console.log(ENV);

    // TODO: REPLACE THIS WITH A SERVICE ???
    // create a uploader with options
    var rootFolder = '';
    if (ENV.status === 'development'){
      rootFolder = '.';
    }else{
      rootFolder = '/srv/www/secure-upload';
    }
    var uploader = $scope.uploader = new FileUploader({
        // TODO: Fix this to take development state
        url: ENV.apiEndpoint + 'blobs.php',
        formData: [ // Metadata for the upload with UID (unique for the upload)
          {
            'data': JSON.stringify({
              'rootFolder': rootFolder,
              'batch': false,
              'upload_dir': 0
            }),
            'UID': UIDservice.updateUID(),
            'chunkID': 0,
            'nChunks': 0,
            'isChunk': false,
            'fileName': '',
            'errorChunk': false,
            'needsMerge': false,
            'merge' : ENV.apiEndpoint + 'blobsMerge.php',
          }
        ]
    });

    // Handlers for Uploader Events
    //////////////////////////////////////////////////////////////////////

    uploader.onAfterAddingFile = function(fileItem) {
        if ($scope.uploader.queue.length > 5){
            console.log("uploading too many files...");
            $scope.queueMessage = 'Uploading too many files can make the job failed...';
        }else{
            console.log("back to normal...");
            $scope.queueMessage = '';
        }
      //console.info('onAfterAddingFile', fileItem);
      // Include metadata to the upload
      // fileItem.formData[0].data = {
      //   'rootFolder': '/srv/www/secure-upload',
      //   'batch': true,
      //   'upload_dir': 0
      // };

    };

    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
      // Fired when the filters return false
      //console.info('When adding a file failed', item, filter, options);
      // if (!$scope.excelRight){
      //   $scope.infoMessage = ' Excel Sheet should be loaded first';
      //   $scope.errorsExist = true;
      //   $scope.multipleErrors = false;
      //   $scope.messages = [];
      // }
    };

    uploader.onBeforeUploadItem = function(item) {
        // var itemsinQueue = $scope.uploader.queue;
        // if (itemsinQueue.length !== $scope.files.length){
        //   console.log('cancelling...');
        //   item.cancel();
        //
        //   $scope.infoMessage = ' Some files are missing';
        //   $scope.errorsExist = true;
        //   $scope.multipleErrors = false;
        //   $scope.messages = [];
        // }else{
        //   $scope.errorsExist = false;
        // }
    };


    uploader.onCompleteItem = function(fileItem, answer, status, headers) {
      //console.info('Complete', xhr, item, response);
      //if ($scope.chunksFailed.length === 0){
        if (answer.state === 'Success'){
          console.info(answer);
          PostMessageService.messages(answer.response);
        }else if (answer.state === 'Error'){
          console.error(answer);
          $scope.infoMessage = ' Error uploading the following files';
          $scope.errorsExist = true;
          $scope.multipleErrors = true;
          $scope.messages.push(answer.response);
        }else{
          console.log('NO SUCCESS NO ERROR', answer);
          console.log('Asume chunks this was one of the chunks that failed');
          $scope.chunksFailed.push(answer.chunkID);
          $scope.infoMessage = ' Error uploading to the server';
          $scope.errorsExist = true;
          $scope.multipleErrors = true;
          $scope.messages.push(fileItem.fileName);

        }
      // }else{
      //   // We retry to send the missing chunks
      //   $scope.infoMessage = ' Error creating file on the server. We will attempt to resend parts of the file';
      //   $scope.errorsExist = true;
      //   $scope.multipleErrors = false;
      //   $scope.messages = [];
      //   console.log('SOME CHUNKS THAT FAILED',$scope.chunksFailed);
      //   //uploader.uploadChunks(fileItem, $scope.chunksFailed);
      //   $scope.errorsExist = false;
      //   $scope.chunksFailed = [];
      // }
    };

    uploader.onCompleteAll = function() {

      // We restore the UID for the isolates to create a new bath folder on the
      // server
      // if (!$scope.errorsExist){
         console.info('Complete All');
      //   // When we get answer from all files we send the paths
         PostMessageService.outgoing();
         $scope.uploader.formData[0].UID = UIDservice.updateUID();
      // }else{
      //   console.log('Errors uploading some files');
      // }
    };

    uploader.onErrorItem = function(fileItem, response, status, headers) {
      //console.info('Complete', xhr, item, response);
      console.log(headers);
      $scope.infoMessage = ' Server is not responding when uploading files';
      $scope.errorsExist = true;
      $scope.multipleErrors = false;
      $scope.messages = [];
    };

    uploader.onCompleteChunk = function (fileItem, response, status, headers){
      if (response.state === 'Error' && response.response === 'Moving chunk'){
        // $scope.infoMessage = ' Error creating the file on the server.';
        // $scope.errorsExist = true;
        // $scope.multipleErrors = false;
        // $scope.messages = [];
        $scope.chunksFailed.push(response.chunkID);
      }
    };

    uploader.onErrorChunk = function (fileItem, response, status, headers){
      console.log('onErrorChunk', response);
      $scope.infoMessage = ' Error creating file on the server. We will attempt to resend parts of the file';
      $scope.errorsExist = true;
      $scope.multipleErrors = false;
      $scope.messages = [];
      console.log('SOME CHUNKS THAT FAILED',$scope.chunksFailed);
      //uploader.uploadChunks(fileItem, $scope.chunksFailed);
      $scope.errorsExist = false;
      $scope.chunksFailed = fileItem.blobsFailed;
    };

    $scope.$on('uploadAllFromIframe', function(){
        uploader.uploadAll();
    //   if (uploader.queue.length !== $scope.files.length){
    //     console.log('Files missing on the queue');
    //   }else{
    //     uploader.uploadAll();
    //   }
    });


  });
