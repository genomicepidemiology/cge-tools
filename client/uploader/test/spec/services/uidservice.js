'use strict';

describe('Service: UIDservice', function () {

  // load the service's module
  beforeEach(module('batchUploadApp'));

  // instantiate service
  var Uidservice;
  beforeEach(inject(function (_UIDservice_) {
    Uidservice = _UIDservice_;
  }));

  it('should do something', function () {
    expect(!!Uidservice).toBe(true);
  });

});
