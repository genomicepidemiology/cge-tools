'use strict';

describe('Service: GeoLocationService', function () {

  // load the service's module
  beforeEach(module('batchUploadApp'));

  // instantiate service
  var Geolocationservice;
  beforeEach(inject(function (_GeoLocationService_) {
    Geolocationservice = _GeoLocationService_;
  }));

  it('should do something', function () {
    expect(!!Geolocationservice).toBe(true);
  });

});
