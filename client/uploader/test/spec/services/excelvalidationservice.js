'use strict';

describe('Service: ExcelvalidationService', function () {

  // load the service's module
  beforeEach(module('batchUploadApp'));

  // instantiate service
  var Excelvalidationservice;
  beforeEach(inject(function (_ExcelvalidationService_) {
    Excelvalidationservice = _ExcelvalidationService_;
  }));
  
  // valid test isolate
  var isolate = {
    'sample_name': '',
    'group_name': '',
    'file_names': 'test.txt',
    'sequencing_platform': '', 
    'sequencing_type': '', 
    'pre_assembled': 'yes',
    'sample_type': 'isolate', // default value in pipeline 1.1
    'organism': 'unknown',// default value in pipeline 1.1
    'strain': '',
    'subtype': '',
    'country': 'denmark',
    'region': '',
    'city': '',
    'zip_code': '',
    'longitude': '',
    'latitude': '',
    'location_note': '',
    'isolation_source': 'unknown',
    'source_note': '',
    'pathogenic': 'unknown',// default value in pipeline 1.1
    'pathogenicity_note': '',
    'collection_date': '2015-01-01',
    'collected_by': '',
    'usage_restrictions': 'private', // default value in pipeline 1.1
    'release_date': '',
    'email_address': '',
    'notes': ''
  };



  it('validating valid isolate', function () {
    expect(Excelvalidationservice.isolate(isolate, 1, 1, [])).toEqual({message:'', errors: 0});
  });
  
  
  it('testing expected filenumbers when sequencing_type = single on different sequencing_platforms', function () {
    isolate.pre_assembled = 'no';
    isolate.sequencing_platform = 'Illumina';
    isolate.sequencing_type = 'single';
    isolate.file_names = 'test1.txt';
    expect(Excelvalidationservice.isolate(isolate, 1, 1, [])).toEqual({message: '', errors: 0});

    isolate.file_names = 'test1.txt test2.txt';
    isolate.sequencing_platform = 'ABI SOLiD';
    expect(Excelvalidationservice.isolate(isolate, 1, 1, [])).toEqual({ message: '', errors: 0});
    
    isolate.file_names = 'test1.txt';
    isolate.sequencing_platform = 'LS454';
    expect(Excelvalidationservice.isolate(isolate, 1, 1, [])).toEqual({ message: '', errors: 0});
    
    isolate.file_names = 'test1.txt';
    isolate.sequencing_platform = 'Ion Torrent';
    expect(Excelvalidationservice.isolate(isolate, 1, 1, [])).toEqual({ message: '', errors: 0});
    
    /* in future versions the uplaoder should be able to handle combining the ion torrent files into one
    isolate.file_names = 'test1.txt test2.txt test3.txt';
    isolate.sequencing_platform = 'Ion Torrent';
    expect(Excelvalidationservice.isolate(isolate, 1, 1, [])).toEqual({ message: '', errors: 0});
    */
  });
  it('testing expected filenumbers when sequencing_type = paired on different sequencing_platforms', function () {
    // paired
    isolate.sequencing_platform = 'Illumina';
    isolate.sequencing_type = 'paired';
    isolate.file_names = 'test1.txt test2.txt';
    expect(Excelvalidationservice.isolate(isolate, 1, 1, [])).toEqual({message: '', errors: 0});

    isolate.file_names = 'test1.txt test2.txt test3.txt test4.txt';
    isolate.sequencing_platform = 'ABI SOLiD';
    expect(Excelvalidationservice.isolate(isolate, 1, 1, [])).toEqual({ message: '', errors: 0});
    
    isolate.file_names = 'test1.txt';
    isolate.sequencing_platform = 'LS454';
    expect(Excelvalidationservice.isolate(isolate, 1, 1, [])).toEqual({ message: '', errors: 0});
    
    isolate.file_names = 'test1.txt';
    isolate.sequencing_platform = 'Ion Torrent';
    expect(Excelvalidationservice.isolate(isolate, 1, 1, [])).toEqual({  message: [ '[Line 1] a format with Sequencing Type "paired" and sequencingPlatform "Ion Torrent"is not supported by our assembler' ], errors: 1});
  });
  it('testing expected filenumbers when sequencing_type = mate-paired on different sequencing_platforms', function () {
    // mate-paired
    isolate.sequencing_type = 'mate-paired';
    isolate.sequencing_platform = 'ABI SOLiD';
    isolate.file_names = 'test1.txt test2.txt test3.txt test4.txt';
    expect(Excelvalidationservice.isolate(isolate, 1, 1, [])).toEqual({ message: '', errors: 0});
    
    isolate.file_names = 'test1.txt test2.txt';
    isolate.sequencing_platform = 'Illumina';
    expect(Excelvalidationservice.isolate(isolate, 1, 1, [])).toEqual({  message: [ '[Line 1] a format with Sequencing Type "mate-paired" and sequencingPlatform "Illumina"is not supported by our assembler' ], errors: 1});

    isolate.file_names = 'test1.txt test2.txt';
    isolate.sequencing_platform = 'LS454';
    expect(Excelvalidationservice.isolate(isolate, 1, 1, [])).toEqual({  message: [ '[Line 1] a format with Sequencing Type "mate-paired" and sequencingPlatform "LS454"is not supported by our assembler' ], errors: 1});
    
    isolate.file_names = 'test1.txt test2.txt';
    isolate.sequencing_platform = 'Ion Torrent';
    expect(Excelvalidationservice.isolate(isolate, 1, 1, [])).toEqual({  message: [ '[Line 1] a format with Sequencing Type "mate-paired" and sequencingPlatform "Ion Torrent"is not supported by our assembler' ], errors: 1});


  });
    
  
  
  it('rejecting unvalid isolate. if isolate is not preassembled then sequencing_platform and sequencing_type is required.', function () {
    isolate.pre_assembled = 'no';
    isolate.sequencing_platform = '';
    isolate.sequencing_type = '';
    isolate.file_names = 'test1.txt test2.txt';
    expect(Excelvalidationservice.isolate(isolate, 1, 1, [])).toEqual({message: [ '[Line 1] Sequencing Platform missing', '[Line 1] Sequencing Type missing' ], errors: 2});
  });

  it('rejecting unvalid isolate. if isolate is preassembled then only a single contig file is expected.', function () {
    isolate.pre_assembled = 'yes';
    isolate.sequencing_platform = '';
    isolate.sequencing_type = '';
    isolate.file_names = 'test1.txt test2.txt';
    expect(Excelvalidationservice.isolate(isolate, 1, 1, [])).toEqual({ message: [ '[Line 1] when preAssembled "yes" a single file is expected' ], errors: 1});
  });
  
  it('rejecting unvalid isolate. faulty date.', function () {

    isolate.file_names = 'test1.txt';
    isolate.collection_date = '2015-13-01';
    expect(Excelvalidationservice.isolate(isolate, 1, 1, [])).toEqual({ message: [ '[Line 1] Invalid format for collection date' ], errors: 1});
  });
  

});
