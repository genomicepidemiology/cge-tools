'use strict';

describe('Service: ExcelParserService', function () {

  // load the service's module
  beforeEach(module('batchUploadApp'));

  // instantiate service
  var Excelparserservice;
  beforeEach(inject(function (_ExcelParserService_) {
    Excelparserservice = _ExcelParserService_;
  }));

  it('should do something', function () {
    expect(!!Excelparserservice).toBe(true);
  });

});
