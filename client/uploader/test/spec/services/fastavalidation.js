'use strict';

describe('Service: FastaValidation', function () {

  // load the service's module
  beforeEach(module('batchUploadApp'));

  // instantiate service
  var fastavalidation;
  beforeEach(inject(function (_FastaValidation_) {
    fastavalidation = _FastaValidation_;
  }));

  it('should do something', function () {
    expect(!!fastavalidation).toBe(true);
  });

});
