describe('testing the batchuploader', function() {
  it('download metadatasheet', function() {
    browser.get('http://localhost:9000/');

    //element(by.id('downloadTemplate')).click();

  });
  
  var path = require('path');

  it(' upload valid metadata', function() {
    var fileExcelToUpload = 'metadata_template_v3TEST.xlsx';
    absolutePath = path.resolve(__dirname, fileExcelToUpload);
    expect(element(by.id('tempUplCorrect')).isDisplayed()).toBeFalsy();
    element(by.id('fileInput')).sendKeys(absolutePath);
    browser.driver.sleep(100);
    expect(element(by.id('tempUplCorrect')).isDisplayed()).toBeTruthy();

  });
  
  it(' upload test file and submit', function() {
    var fileToUpload = 'test.txt';
    absolutePath = path.resolve(__dirname, fileToUpload);

    element(by.id('filesUploader')).sendKeys(absolutePath);
    browser.driver.sleep(100);
    expect(element(by.id('itemSuccesfullyUploaded')).isDisplayed()).toBeFalsy();
    element(by.id('submitFiles')).click();
    browser.driver.sleep(5000);
    expect(element(by.id('itemSuccesfullyUploaded')).isDisplayed()).toBeTruthy();
  });
});