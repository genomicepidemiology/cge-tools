CGE Tools: Batch Upload
===============

CGE Tool to upload several isolates

The last working version can be found [here](https://cge.cbs.dtu.dk/tools/client/batch/index.html)

Documentation
------
The following technologies have been used:


Deployment
------
To create a distribution version of the app be sure to commit all your changes
before you run `grunt build`. This command produces a folder called built and
push it to a new branch on the git repository called production-batch-client.

To deploy the map visualization app use the following command:

`git clone git@bitbucket.org:genomicepidemiology/cge-tools.git -b production-natch-client --single-branch batch`

in the client folder and use `get pull` to get new updates.

Errors
------
When executing `grunt serve`, some errors can be found mainly due to some
libraries missing when Grunt tries to install them via `grunt bower-install`.
The solution to this is to inject manually the component in the index.html file.

Authors
------

* Jose Luis Bellod Cisneros
