'use strict';

/**
 * @ngdoc service
 * @name batchUploadApp.fastavalidation
 * @description
 * # fastavalidation
 * Service in the batchUploadApp.
 */
angular.module('batchUploadApp')
  .service('FastaValidation', function fastavalidation($q) {

    // var extensions = [];
    var that = this;
    // AngularJS will instantiate a singleton by calling "new" on this function
    this.call = function (file, assembled) {
      var deferred = $q.defer();
      var promise = deferred.promise;
      var reader = new FileReader();
      console.log(reader);
      reader.onload = function(event) {
        console.log(event);
        if (event.target.readyState === FileReader.DONE) {
          var fileAssembled = event.target.result === '>';
          // first char == @ => FASTQ elif char == >  => FASTA
          // check if metadata pre_assembled == yes or no
          var answer = {};
          if (fileAssembled && assembled === 'yes'){
            console.log('File type right...');
            answer.correct = true;
          }else if (!fileAssembled && assembled === 'yes'){
            answer.correct = false;
            answer.message = 'FASTQ file provided and pre_assembled option set to YES';
          }else if (fileAssembled && assembled === 'no'){
            answer.correct = false;
            answer.message = 'FASTA file provided and pre_assembled option set to NO';
          }else{
            answer.correct = true;
          }
          console.log(answer.message);
          deferred.resolve(answer);
        }
      };
      var blob = file.slice(0,1);
      reader.readAsText(blob);

      return promise;
    };

  });
