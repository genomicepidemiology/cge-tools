'use strict';

angular.module('batchUploadApp')
.service('ExcelvalidationService', function ExcelvalidationService($filter) {


   // AngularJS will instantiate a singleton by calling 'new' on this function

    // Check if the row in the excel file is empty
    this.emptyRow = function (isolate) {

       var isolateTemplate = [
          'sample_name',
          'group_name',
          'file_names',
          'sequencing_platform',
          'sequencing_type',
          'pre_assembled',
          'sample_type',
          'organism',
          'strain',
          'subtype',
          'country',
          'region',
          'city',
          'zip_code',
          'longitude',
          'latitude',
          'location_note',
          'isolation_source',
          'source_note',
          'pathogenic',
          'pathogenicity_note',
          'collection_date',
          'collected_by',
          'usage_restrictions',
          'release_date',
          'email_address',
          'notes'
       ];
       var emptyRow = 0;
       isolateTemplate.forEach(function(d){
          if (d !== 'upload_dir'){
            //if (isolate[d] === '' || isolate[d] === 0){
            if (( d !== 'release_date' && isolate[d]) || isolate[d] === 0){
               emptyRow+=1;
            }
          }
       });
       return (isolateTemplate.length === emptyRow);

    };

    this.isolate = function (isolate, line, checkFiles, fileNames) {

      var sampleType = ['metagenomic', 'isolate'];
      var sequencingPlatform = ['LS454', 'Illumina', 'Ion Torrent', 'ABI SOLiD', 'unknown'];
      var sequencingType = ['single', 'paired', 'mate-paired', 'unknown'];
      var preAssembled = ['yes', 'no'];
      var isolationSource = ['human', 'water', 'food', 'animal', 'other', 'laboratory', 'unknown'];
      var pathogenic = ['yes', 'no', 'unknown'];
      var collectionDateFormatA = d3.time.format('%Y-%m-%d');
      var collectionDateFormatB = d3.time.format('%Y-%m');
      var collectionDateFormatC = d3.time.format('%Y');
      var usageRestrictions = ['private', 'public'];

      var str = '';
      var errorMessages = [];
      var errors = 0;

      if (checkFiles && isolate.file_names === ''){
         str =  '[Line ' + line.toString() + '] ' + 'Isolate files missing';
         errorMessages.push(str);
         errors+=1;
      }

      var isolateFiles = isolate.file_names.split(' ');
      var tempFileNames = angular.copy(fileNames);
      var noEmptyFiles = true;
      isolateFiles.forEach(function(fileName){
        if (tempFileNames.indexOf(fileName) !== -1){
          if (fileName !== ''){
            str = '[Line ' + line.toString() + '] ' + 'File ('+ fileName +') already included';
            errorMessages.push(str);
            errors+=1;
          }else{
            if (noEmptyFiles){
              str =  '[Line ' + line.toString() + '] ' + 'White spaces in file_names field';
              errorMessages.push(str);
              errors+=1;
              noEmptyFiles = false; // Check that we get in onle once
            }
          }
        }else{
          tempFileNames.push(fileName);
        }
      });
		

      if (sequencingPlatform.indexOf(isolate.sequencing_platform) === -1){
        if (isolate.sequencing_platform.trim() !== ''){
          str = '[Line ' + line.toString() + '] ' + 'Sequencing Platform "'+ isolate.sequencing_platform +'" is not a valid option';
          errorMessages.push(str);
          errors+=1;
        }
          // pipeline 1.1 - sequencing platform only mandatory if pre assembled is 'no' else, default value for sequencing platform is 'unknown'
        else {
          if(isolate.pre_assembled === 'no'){
            str = '[Line ' + line.toString() + '] ' + 'Sequencing Platform missing';
            errorMessages.push(str);
            errors+=1;
          }
          else {
            isolate.sequencing_platform = 'unknown';
          }
        }
      }

      if (sequencingType.indexOf(isolate.sequencing_type ) === -1){
        if (isolate.sequencing_type.trim() !== ''){
          str = '[Line ' + line.toString() + '] ' + 'Sequencing Type "'+ isolate.sequencing_type  +'" is not a valid option';
          errorMessages.push(str);
          errors+=1;
        }
          // pipeline 1.1 - sequencing platform only mandatory if pre assembled is 'no' else, default value for sequencing type is 'unknown'
        else {
          if (isolate.pre_assembled === 'no'){
            str = '[Line ' + line.toString() + '] ' + 'Sequencing Type missing';    
            errorMessages.push(str);
            errors+=1;
          }
          else{
            isolate.sequencing_type = 'unknown';
          }
        }
      }
      if (isolate.pre_assembled === 'yes'){ 
        // the number of files doesn't have to match the sequencingType since they have been pre assembled and only the contig file is expected
        console.log(isolateFiles.length);
        if(isolateFiles.length !== 1){
        str = '[Line ' + line.toString() + '] ' + 'when preAssembled "'+ isolate.pre_assembled  +'" a single file is expected';
        errorMessages.push(str);
        errors+=1;
        }
      }
      else{ // check if number of files matches sequencingType input combined with sequencingPlatform input
        if(isolate.sequencing_type === 'single'){
          if(isolate.sequencing_platform === 'Illumina' || isolate.sequencing_platform === 'LS454'){
            if(isolateFiles.length !== 1){
              str = '[Line ' + line.toString() + '] ' + 'When Sequencing Type "'+ isolate.sequencing_type  +'" and sequencingPlatform "' + isolate.sequencing_platform +'" a single file is expected';
              errorMessages.push(str);
              errors+=1;
            }
          }
          else if (isolate.sequencing_platform === 'ABI SOLiD') {
            if (isolateFiles.length !== 2){
              str = '[Line ' + line.toString() + '] ' + 'When Sequencing Type "'+ isolate.sequencing_type  +'" and sequencingPlatform "' + isolate.sequencing_platform +'" two files are expected';
              errorMessages.push(str);
              errors+=1;
            }
          }
          else if (isolate.sequencing_platform === 'Ion Torrent') {
            if(isolateFiles.length !==1){
              str = '[Line ' + line.toString() + '] ' + 'When Sequencing Type "'+ isolate.sequencing_type  +'" and sequencingPlatform "' + isolate.sequencing_platform +'"one file is expected';
              errorMessages.push(str);
              errors+=1;
            }
          }
          else{
            str = '[Line ' + line.toString() + '] ' + 'a format with Sequencing Type "'+ isolate.sequencing_type  +'" and sequencingPlatform "' + isolate.sequencing_platform +'"is not supported by our assembler';
            errorMessages.push(str);
            errors+=1;
          }
        }
        else if (isolate.sequencing_type === 'paired'){
          if(isolate.sequencing_platform === 'Illumina'){
            if(isolateFiles.length !== 2){
              str = '[Line ' + line.toString() + '] ' + 'When Sequencing Type "'+ isolate.sequencing_type  +'" and sequencingPlatform "' + isolate.sequencing_platform +'" two files are expected';
              errorMessages.push(str);
              errors+=1;
            }
          }
          else if(isolate.sequencing_platform === 'ABI SOLiD'){
            if(isolateFiles.length !== 4){
              str = '[Line ' + line.toString() + '] ' + 'When Sequencing Type "'+ isolate.sequencing_type  +'" and sequencingPlatform "' + isolate.sequencing_platform +'" four files are expected';
              errorMessages.push(str);
              errors+=1;
            }
          }
          else if(isolate.sequencing_platform === 'LS454'){
            if(isolateFiles.length !== 1){
              str = '[Line ' + line.toString() + '] ' + 'When Sequencing Type "'+ isolate.sequencing_type  +'" and sequencingPlatform "' + isolate.sequencing_platform +'" a single file is expected';
              errorMessages.push(str);
              errors+=1;
            }
          }
          else{
            str = '[Line ' + line.toString() + '] ' + 'a format with Sequencing Type "'+ isolate.sequencing_type  +'" and sequencingPlatform "' + isolate.sequencing_platform +'"is not supported by our assembler';
            errorMessages.push(str);
            errors+=1;
          }
        }
        else if (isolate.sequencing_type === 'mate-paired'){
          if(isolate.sequencing_platform === 'ABI SOLiD'){
            if(isolateFiles.length !== 4){
              str = '[Line ' + line.toString() + '] ' + 'When Sequencing Type "'+ isolate.sequencing_type  +'" and sequencingPlatform "' + isolate.sequencing_platform +'" two files are expected';
              errorMessages.push(str);
              errors+=1;
            }
          }
          else{
            str = '[Line ' + line.toString() + '] ' + 'a format with Sequencing Type "'+ isolate.sequencing_type  +'" and sequencingPlatform "' + isolate.sequencing_platform +'"is not supported by our assembler';
            errorMessages.push(str);
            errors+=1;
          }
        }
      }
        

      if (preAssembled.indexOf(isolate.pre_assembled) === -1){
        if (isolate.pre_assembled.trim() !== ''){
          str = '[Line ' + line.toString() + '] ' + 'Pre Assembled "'+ isolate.pre_assembled +'" is not a valid option';
        }
        else{
          str = '[Line ' + line.toString() + '] ' + 'Pre Assembled missing';
        }
        errorMessages.push(str);
        errors+=1;
       }
      
      // removed from batch upload in pipeline 1.1 (should return in 2.0)
      /*if (sampleType.indexOf(isolate.sample_type ) === -1){
         if (isolate.sample_type.trim() !== ''){
           str = '[Line ' + line.toString() + '] ' + 'Sample type "'+ isolate.sample_type  +'" is not a valid option';
         }else{
           str = '[Line ' + line.toString() + '] ' + 'Sample type missing';
         }
         errorMessages.push(str);
         errors+=1;
      }*/
      
        // not mandatory in pipeline 1.1
        /*if (isolate.organism.trim() === ''){
          str = '[Line ' + line.toString() + '] ' + 'Organism missing';
          errorMessages.push(str);
          errors+=1;
        }*/

        if (isolate.country.trim() === ''){
           str = '[Line ' + line.toString() + '] ' + 'Country not present';
           errorMessages.push(str);
           errors+=1;
        }

        if (isolationSource.indexOf(isolate.isolation_source) === -1){
           if (isolate.isolation_source.trim() !== ''){
             str = '[Line ' + line.toString() + '] ' + 'Isolation Source "'+ isolate.isolation_source  +'" is not a valid option';
           }else{
             str = '[Line ' + line.toString() + '] ' + 'Isolation Source missing';
           }
           errorMessages.push(str);
           errors+=1;
        }
  
        // not mandatory in pipeline 1.1 
        if (pathogenic.indexOf(isolate.pathogenic ) === -1){
           if (isolate.pathogenic.trim() !== ''){
             str = '[Line ' + line.toString() + '] ' + 'Pathogenic "'+ isolate.pathogenic +'" is not a valid option';
             errorMessages.push(str);
             errors+=1;
           }else{
             //str = '[Line ' + line.toString() + '] ' + 'Pathogenic missing';
           }
        }

       var auxDateA = collectionDateFormatA.parse(isolate.collection_date);
       var auxDateB = collectionDateFormatB.parse(isolate.collection_date);
       var auxDateC = collectionDateFormatC.parse(isolate.collection_date);
       
       if (auxDateA != null && collectionDateFormatA(auxDateA) === isolate.collection_date || 
          auxDateB != null && collectionDateFormatB(auxDateB) === isolate.collection_date ||
          auxDateC != null && collectionDateFormatC(auxDateC) === isolate.collection_date){
          // if one of the dates fits the format there should be no error
       }
       else{
          str = '[Line ' + line.toString() + '] ' + 'Invalid format for collection date';
          errorMessages.push(str);
          errors+=1;
       }

       var auxDateA = collectionDateFormatA.parse(isolate.release_date);
       if (auxDateA === null && isolate.release_date.trim() !== ''){
          str = '[Line ' + line.toString() + '] ' + 'Invalid format for release_date date';
          errorMessages.push(str);
          errors+=1;
       }

      // removed from batch upload in pipeline 1.1 should return in 2.0
       /*if (usageRestrictions.indexOf(isolate.usage_restrictions) === -1){
          if (isolate.usage_restrictions.trim() !== ''){
            str = '[Line ' + line.toString() + '] ' + 'Usage restrictions "'+ isolate.usage_restrictions +'" is not a valid option';
          }else{
            str = '[Line ' + line.toString() + '] ' + 'Usage restrictions missing';
          }
          errorMessages.push(str);
          errors+=1;
       }*/

       if (errors === 0){
          return {
             message: '',
             errors: errors
          };
       }else{
          return {
             message: errorMessages ,
             errors : errors
          };
       }
    };

 });
