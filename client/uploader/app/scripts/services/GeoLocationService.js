'use strict';

angular.module('batchUploadApp')
  .service('GeoLocationService', function GeoLocationService($q) {


   var openStreetMapGeocoder = GeocoderJS.createGeocoder('openstreetmap');

   this.getLatLngFromLocation = function (isolate, errors){

      // Task that will finish in the future
      var deferred = $q.defer();
      var promise = deferred.promise;
      var locationString  = '';

      // TODO: Add ZIP CODE to the location search
      if (isolate.longitude === '' || isolate.latitude === ''){
         if (isolate.region !== '') {
            locationString = isolate.city + ', ' + isolate.region + ', ' + isolate.country;
         }else{
            locationString = isolate.city + ', ' + isolate.country;
         }
         console.log('Asking for address...', locationString);
         openStreetMapGeocoder.geocode(locationString, function(result) {
            console.log('Coordinates resolved...', locationString, result);
            if (result.length > 0){
               isolate.longitude = result[0].longitude;
               isolate.latitude =  result[0].latitude;
            }else{
               //isolate.locationError = true;
               errors.messages.push(
                  'Unknown location in isolate number: ' + isolate.sample_name
               );
               errors.nErrors += 1;
            }
            deferred.resolve(isolate);
         });

       }else{
         // Resolve already
         console.log('we shouldnt see this');
         deferred.resolve(isolate);
       }
       return promise;
   };

   this.getLocationFromLatLng = function (isolate){
      // Task that will finish in the future
      var deferred = $q.defer();
      var promise = deferred.promise;
      openStreetMapGeocoder.geodecode(isolate.latitude, isolate.longitude, function(result) {
        console.log(result);
        deferred.resolve(isolate);
      });
      return promise;
   };

  });
