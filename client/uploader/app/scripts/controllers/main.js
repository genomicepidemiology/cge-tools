'use strict';

angular.module('batchUploadApp')
  .controller('MainCtrl', function ($scope, FileUploader, PostMessageService, ENV, UIDservice, FastaValidation, Notification) {


    //////////////
    ////////////// TODO: FIX time between Batchs
    ////////////// TODO: WARNING when uploading more than 150 isolates per day
    ////////////// TODO: ... Ask martin again about this
    //////////////

    ///////////////////////////
    // Scope variables

    // General variables
    $scope.isCollapsed = true;
    $scope.infoMessage = '';       // Initially no info message
    $scope.errorsExist = false;
    $scope.multipleErrors = false;
    $scope.ringTrial = false;
    
     $scope.$watch('ringTrial', function(newValue, oldValue) {
       if(newValue!=oldValue)
       {
         $scope.uploader.formData[0].ringTrial = newValue;
       }
       
   });

    $scope.excelURL = ENV.apiEndpoint + 'metadataform.xlsx';
    $scope.excelInfoText = 'Download the <a href="'+ ENV.apiEndpoint + 'metadataform.xlsx">'
                            +'template here<a/> and fill it out with ' +
                            'information about your isolates: Date, Country...';
    $scope.missingFiles = [];
    $scope.chunksFailed = [];

    $scope.downloadExcelFile = function(){
       console.log($scope.excelURL);
       var element = angular.element('#excelLink');
          element.attr({
             href: $scope.excelURL,
             target: '_self',
             download:'metadataform.xlsx'
          })[0].click();
    };

    console.log(ENV);

    // TODO: REPLACE THIS WITH A SERVICE ???
    // create a uploader with options
    // var url = ENV.apiEndpoint;
    // if (ENV.status === 'development'){
    //   url = ENV.apiEndpoint + 'blobs.php';
    // }else{
    //   url = ENV.apiEndpoint + '/upload_blobs.php';
    // }
    var uploader = $scope.uploader = new FileUploader({
        // TODO: Fix this to take development state
        url: ENV.apiEndpoint + 'blobs.php',
        formData: [ // Metadata for the upload with UID (unique for the upload)
          {
            'data': {},
            'UID': UIDservice.updateUID(),
            'chunkID': 0,
            'nChunks': 0,
            'isChunk': false,
            'fileName': '',
            'errorChunk': false,
            'needsMerge': false,
            'merge' : ENV.apiEndpoint + 'blobsMerge.php',
            'ringTrial': false,
          }
        ],
        filters: [{
          name: 'allFilesExist',
          fn: function (item) {
                if (($scope.files.indexOf(item.name) === -1 ) &&
                      !(item.isChunk)){
                  console.log('filter failed');

                  Notification.warning({message: item.name + ' could not be found in the excel template', delay: 5000,positionY: 'top', positionX: 'right'});

                  return false;
                }else{
                  //$scope.errorsExist = false; // This removes the error messages
                  return true;
                }
            },
          },{
            name: 'duplicateFiles',
            fn: function (item) {
               // Check if same file was uploaded more than one time
               var isolates = [];
               $scope.uploader.getNotUploadedItems().forEach(function(d){
                  isolates.push(d.file.name);
               });
               if (isolates.length !== 0 && (isolates.indexOf(item.name) !== -1 )){
                  Notification.warning({message: item.name +' is already added to the queue', delay: 5000,positionY: 'top', positionX: 'right'});
                  return false;
               }else{
                  //$scope.errorsExist = false; // This removes the error messages
                  return true;
               }
            }
          }
          // ,{
          //   name: 'assembledCheck',
          //   fn: function (item) {
          //     console.log('Checking FASTA...' ,item, $scope.metadata[item.name].pre_assembled);
          //     //TODO: check FASTA or FASTQ file
          //     // first char == @ => FASTQ elif char == >  => FASTA
          //     // check if metadata pre_assembled == yes or no
          //     FastaValidation
          //       .call(item, $scope.metadata[item.name].pre_assembled)
          //       .then(function(answer){
          //         if (!answer.correct){
          //           $scope.errors+=1;
          //           $scope.messages.push(answer.message);
          //           $scope.errorsExist = true;
          //           $scope.multipleErrors = true;
          //           console.log('Error...');
          //           return false;
          //         }else{
          //           console.log('True...');
          //           return true;
          //         }
          //       });
          //   }
          // }
        ]
    });

    // Handlers for Uploader Events
    //////////////////////////////////////////////////////////////////////

    uploader.onAfterAddingFile = function(fileItem) {
      //console.info('onAfterAddingFile', fileItem);
      // Include metadata to the upload
      fileItem.formData[0].data = JSON.stringify(
        $scope.metadata[fileItem.file.name]
      );
    };

    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
      // Fired when the filters return false
      //console.info('When adding a file failed', item, filter, options);
      if (!$scope.excelRight){
        $scope.infoMessage = ' Excel Sheet should be loaded first';
        $scope.errorsExist = true;
        $scope.multipleErrors = false;
        $scope.messages = [];
      }
    };

    uploader.onBeforeUploadItem = function(item) {
        var itemsinQueue = $scope.uploader.queue;
        if (itemsinQueue.length !== $scope.files.length){
          console.log('cancelling...');
          item.cancel();

          $scope.infoMessage = ' Some files are missing';
          $scope.errorsExist = true;
          $scope.multipleErrors = false;
          $scope.messages = [];
        }else{
          $scope.errorsExist = false;
        }
    };


    uploader.onCompleteItem = function(fileItem, answer, status, headers) {
      //console.info('Complete', xhr, item, response);
      //if ($scope.chunksFailed.length === 0){
        if (answer.state === 'Success'){
          console.info(answer);
          PostMessageService.messages(answer.response);
        }else if (answer.state === 'Error'){
          console.error(answer);
          $scope.infoMessage = ' Error uploading the following files';
          $scope.errorsExist = true;
          $scope.multipleErrors = true;
          $scope.messages.push(answer.response);
        }else{
          console.log('NO SUCCESS NO ERROR', answer);
          console.log('Asume chunks this was one of the chunks that failed');
          $scope.chunksFailed.push(answer.chunkID);
          $scope.infoMessage = ' Error uploading to the server';
          $scope.errorsExist = true;
          $scope.multipleErrors = true;
          $scope.messages.push(fileItem.fileName);

        }
      // }else{
      //   // We retry to send the missing chunks
      //   $scope.infoMessage = ' Error creating file on the server. We will attempt to resend parts of the file';
      //   $scope.errorsExist = true;
      //   $scope.multipleErrors = false;
      //   $scope.messages = [];
      //   console.log('SOME CHUNKS THAT FAILED',$scope.chunksFailed);
      //   //uploader.uploadChunks(fileItem, $scope.chunksFailed);
      //   $scope.errorsExist = false;
      //   $scope.chunksFailed = [];
      // }
    };

    uploader.onCompleteAll = function() {

      // We restore the UID for the isolates to create a new bath folder on the
      // server
      if (!$scope.errorsExist){
        console.info('Complete All');
        // When we get answer from all files we send the paths
        PostMessageService.outgoing();
        $scope.uploader.formData[0].UID = UIDservice.updateUID();
      }else{
        console.log('Errors uploading some files');
      }
    };

    uploader.onErrorItem = function(fileItem, response, status, headers) {
      //console.info('Complete', xhr, item, response);
      console.log(headers);
      $scope.infoMessage = ' Server is not responding when uploading files';
      $scope.errorsExist = true;
      $scope.multipleErrors = false;
      $scope.messages = [];
    };

    uploader.onCompleteChunk = function (fileItem, response, status, headers){
      if (response.state === 'Error' && response.response === 'Moving chunk'){
        // $scope.infoMessage = ' Error creating the file on the server.';
        // $scope.errorsExist = true;
        // $scope.multipleErrors = false;
        // $scope.messages = [];
        $scope.chunksFailed.push(response.chunkID);
      }
    };

    uploader.onErrorChunk = function (fileItem, response, status, headers){
      console.log('onErrorChunk', response);
      $scope.infoMessage = ' Error creating file on the server. We will attempt to resend parts of the file';
      $scope.errorsExist = true;
      $scope.multipleErrors = false;
      $scope.messages = [];
      console.log('SOME CHUNKS THAT FAILED',$scope.chunksFailed);
      //uploader.uploadChunks(fileItem, $scope.chunksFailed);
      $scope.errorsExist = false;
      $scope.chunksFailed = fileItem.blobsFailed;
    };

    $scope.$on('uploadAllFromIframe', function(){
      if (uploader.queue.length !== $scope.files.length){
        console.log('Files missing on the queue');
      }else{
        uploader.uploadAll();
      }
    });
    
    $scope.checkAndSubmitFiles = function(){
      if (uploader.queue.length !== $scope.files.length){
        var missingFilesInQueue = $scope.files.slice();
        uploader.getNotUploadedItems().forEach( function (item) { 
          if(missingFilesInQueue.indexOf(item.file.name) !== -1) {
            delete missingFilesInQueue[missingFilesInQueue.indexOf(item.file.name)];
          }
        });
        console.log('Files missing on the queue');
        $scope.infoMessage = ' Some of the file(s) specified in the excel template '+
                             'could not be found';
        $scope.messages = missingFilesInQueue;
        $scope.errorsExist = true;
        $scope.multipleErrors = true;
      }else{
        uploader.uploadAll();
      }
    };


  });
