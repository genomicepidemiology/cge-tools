CGE Tools: Map Visualization
===============

CGE Tool to visualize isolates on a map

The last working version can be found [here](https://cge.cbs.dtu.dk/tools/client/map/index.html)

Documentation
------
The following technologies have been used:

* [Leaflet](http://leafletjs.com/)
* [D3](http://d3js.org/)
* [Crossfilter](http://square.githubio/crossfilter/)
* [DC.js](http://dc-js.github.io/dc.js/)
* [AngularJS](https://angularjs.org/)

Documentation of the code can be found [here](XX)

To test the map, [Node.js](http://nodejs.org/), [Grunt](http://gruntjs.com/)
and [Bower](http://bower.io/) should be installed. 

If you are using a Windows operating system, you should use a console emulator.
If you aren't already, then there is a guide to setting up Git hub with Cmder [here]. (http://www.awmoore.com/2015/01/14/coding-in-windows-part-1/)
you will then be able to run all the necessary commands in cmder.

The best option is first to install NodeJS and then execute the following command:

 `npm install -g yo`

after cloning the repository to a folder, you should go to the root directory of the repository.

Then, from the root directory, execute `npm install` then `bower install` 
which will install the necessary dependencies.
Now, you can from the same folder run the `grunt serve` command, to run the local server and open
the map.

NOTE
if you would like to access the local server from other devices and virtual machines.
Then you can do this by:

open the gruntfile.js in the root directory.
locate the hostname: attribute and change its value from 'localhost' to '0.0.0.0'.
when the command `grunt serve` is run, the browser will fail. Because it mistakenly
tries to locate the server at 0.0.0.0:9000. So simply enter `localhost` instead of 0.0.0.0.

then, to connect from another device or virtual machine. 
Enter your computers internal IP address, followed by the port number like so.
xx.xx.xxx.xxx:9000

I assume in this example that the server is hosted on port 9000. 
the port number can be located next to the hostname: attribute but will by default be 9000

Deployment
------
To create a distribution version of the app be sure to commit all your changes
before you run `grunt build`. This command produces a folder called built and
push it to a new branch on the git repository called production-map-client.

To deploy the map visualization app use the following command:

`git clone git@bitbucket.org:genomicepidemiology/cge-tools.git -b production-map-client --single-branch map`

in the client folder and use `get pull` to get new updates.

Errors
------
When executing `grunt serve`, some errors can be found mainly due to some
libraries missing when Grunt tries to install them via `grunt bower-install`.
The solution to this is to inject manually the component in the index.html file.

another solution is to run `git stash` after the `grunt serve` command has been run.
this will overwrite the local version with the working version on git.
Thereby injecting the necessary libraries but also reverting all other changes.
Authors
------

* Jose Luis Bellod Cisneros
* Valentin Ibanez