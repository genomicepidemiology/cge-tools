'use strict';

describe('Directive: mapOutbreaks', function () {

  // load the directive's module
  beforeEach(module('mapVisualizationApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<svg map-outbreaks></svg>');
    element = $compile(element)(scope);
    expect(scope).not.toBe(null);

  }));
});
