'use strict';

describe('Directive: hexbin', function () {

  // load the directive's module
  beforeEach(module('mapVisualizationApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<hexbin></hexbin>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the hexbin directive');
  }));
});
