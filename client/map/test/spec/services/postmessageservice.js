'use strict';

describe('Service: PostMessageService', function () {

  // load the service's module
  beforeEach(module('mapVisualizationApp'));

  // instantiate service
  var PostMessageService;
  beforeEach(inject(function (_PostMessageService_) {
    PostMessageService = _PostMessageService_;
  }));

  it('should do something', function () {
    expect(!!PostMessageService).toBe(true);
  });

});
