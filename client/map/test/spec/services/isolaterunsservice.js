'use strict';

describe('Service: Isolaterunsservice', function () {

  // load the service's module
  beforeEach(module('mapVisualizationApp'));

  // instantiate service
  var Isolaterunsservice;
  beforeEach(inject(function (_Isolaterunsservice_) {
    Isolaterunsservice = _Isolaterunsservice_;
  }));

  it('should do something', function () {
    expect(!!Isolaterunsservice).toBe(true);
  });

});
