CGE Tools: Map Visualization
===============

CGE Tool to manage the server side of the map visualization tool

The last working version can be found [here](https://cge.cbs.dtu.dk/tools/client/map/index.html)

Documentation
------

Deployment
------

Three branches are associated to this folder:

* master: development
* production-map-server: production
* production-batch-server: production

To push new changes to the production branch run:
* `git push remote-server production-map-server`
or
* `git push remote-server-batch production-batch-server`

from the map-prod folder (an isolate repository has been created here in order
  to make the deployment easier)

To deploy the server tools run on the cge machine/folder:
* `git clone git@bitbucket.org:genomicepidemiology/cge-tools.git -b production-map-server --single-branch map`
in the server folder.

* `git clone git@bitbucket.org:genomicepidemiology/cge-tools.git -b production-batch-server --single-branch batch`


Errors
------


Authors
------

* Jose Luis Bellod Cisneros
